import random
import math
import numpy as np

from sklearn.metrics import roc_auc_score
from sklearn.metrics import roc_curve

from .dataset import Dataset
from .tree import Tree, c

class Forest:

    def __init__(self, d: Dataset, nbT : int = 100, method="normal", psi=256, maxHeight=math.ceil(math.log(256,2)), alpha=0.05, eta=0.5):
        """
        Parameters
        ----------
        d : Dataset
            the data
        alpha : float
            the width of the margin
        Returns
        -------
        None.

        """
        random.seed()
        self.dataSet= d
        self.trees = []
        # Hyperparameters
        self.NBTREES = nbT
        self.METHOD = method
        self.MAXSIZESS = psi
        self.MAXHEIGHT = maxHeight
        self.ALPHA = alpha
        self.ETA = eta
        
        
    def __str__(self):
        ret = "FOREST | NBTREES : "+str(self.NBTREES)+ " MAXSIZE : "+str(self.MAXSIZESS)+" ALPHA : "+str(self.ALPHA)+" ETA : "+str(self.ETA)
        for i in range(len(self.trees)):
            ret += "\n"+str(self.trees[i])
        return ret

        
    def build(self):
        """
        Builds the isolation forest
        """
        # Permutation of the ids
        rng = np.random.default_rng()
        nbPoints = self.dataSet.getNbPoints()
        sampleSize = min(self.MAXSIZESS, nbPoints)
        for i in range(self.NBTREES):
            sampleIds = rng.choice(range(self.dataSet.getNbPoints()), sampleSize, replace=False)
            self.trees.append(Tree(self, sampleIds))



    def computeScore(self, x):
        """
        Computes the anomaly scores of a single data point
        using the degrees attached to points in the leaf
        Parameters
        ----------
        x : 1D array of floats
            the coordinates of the data point.
        Returns
        -------
        float
            the anomaly score of the data point in the isolation forest.

        """
        
        if self.METHOD == "modified":
            score = 0.0
            for i in range(self.NBTREES):
                score = score + self.trees[i].computeScoreModified(x)
            return (score / self.NBTREES)
            
        else:
            hx = 0.0
            for i in range(self.NBTREES):
                hx = hx + self.trees[i].pathLength(x, self.trees[i].root, 0)
            hx = hx / self.NBTREES
            nbPoints = self.dataSet.getNbPoints()
            sampleSize = min(self.MAXSIZESS, nbPoints)
            return 2 ** ( -hx / c(sampleSize))
    

    def computeScores(self):
        """
        Computes the anomaly scores of all the data points in the dataset
        Returns
        -------
        1D array of floats
            the anomaly score of each data point.
        """
        return np.apply_along_axis(self.computeScore, 1, self.dataSet.getData())
    
    
    
    def computeAUC(self,scores):
        binData = self.dataSet.binClassesVector()
        ruc = roc_auc_score(binData, scores)
        ns_fpr, ns_tpr, _ = roc_curve(binData, scores)
        return ruc,ns_fpr,ns_tpr
    
    
    