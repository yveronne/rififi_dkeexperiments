#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: SHAMAN
"""

import numpy as np
import pandas as pd
from functools import reduce
import matplotlib.pyplot as plt
import sys
import math

class Dataset:

    def __init__(self, df, header, conv):
        """
        Loads a csv dataset 
        """
        
        us = tuple(range(len(header)))
        self.data = np.loadtxt(df, delimiter=",", converters = conv, skiprows=0, usecols=us) # Is a np.ndarray
        self.nbPoints = len(self.data)
        self.attributes=header[:-1] # Because the last column is the label (abnormal or not)


    def getNbPoints(self):
        return self.nbPoints


    def getData(self, i=None):
        if i is not None:
            return self.data[i]
        else:
            return self.data


    def getClass(self,i):
        classe = None
        if self.supervised:
            classe =  self.data[i][len(self.getAttributes())]
        return classe

    
    def getAnomaliesIndices(self):
        return [i for i in range(self.getNbPoints()) if (self.getClass(i) == 1)]


    def getAttributes(self):
        return self.attributes

        
    def binClassesVector(self):
        res = np.zeros(self.nbPoints)
        j=0
        for i in range(self.nbPoints):
            pt = self.data[i]
            res[j] = int(pt[len(pt)-1])
            j=j+1
        return res


