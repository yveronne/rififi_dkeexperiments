import random

from .dataset import Dataset
from .node import Node

import math
import numpy as np

class Tree :

    def __init__(self, f, ids):
        self.forest = f
        self.dataSet = f.dataSet
        self.ids = ids
        self.endNodes = []
        self.root = self.build(self.ids, 0)
        
    
        
    def __str__(self):
        ret="TREE | IDS : "+str(self.ids)+"\n"
        
        return ret + self.recStr(self.root,0)


    def recStr(self, n,i):
        ret = ""
        if n is not None:
            ret="\t"*i+str(n)+"\n"+self.recStr(n.leftNode,i+1)+"\n"+self.recStr(n.rightNode,i+1)
        return ret
           

    def build(self, idsR, currDepth:int, margins=None):
        """
        Builds the tree

        Parameters
        ----------
        idsR : dict()
            pointIdx => deg
        currDepth : integer
            the current depth of the tree in the building process. 
            Starts from 0 for the root of the tree and is incremented as we go deeper in the tree. 
            Used to stop building the tree when we reach the height limit.
        margins : the lower bound of the margin on each dimension. Modified isolation forest only

        Returns
        -------
        Node
            can be an internal or an external node. An external node has no children and
            is returned when we have only one data point left or when the height limit is 
            reached.

        """
        
        # Build a modified tree
        if self.forest.METHOD == "modified":
            nbPtsNearLeft=0 # Number of points in the left margin
            nbPtsNearRight=0 # Number of points in the right margin
            proxLeftBorder=0 # Value of the left border
            proxRightBorder=0 # Value of the right border
            
            tested = []
            excluded = None
            choices = None

            # Half of the width of the margin (in %)
            proxAreaRange = self.forest.ALPHA / 2
            
            attributes = self.dataSet.getAttributes()
             
            if margins is None:
                margins = [None for _ in range(len(attributes))]

            if len(idsR) <= 1 or currDepth >= self.forest.MAXHEIGHT:
                self.endNodes.append(idsR)
                return Node(idsR, currDepth, None, None, None,None)
            else:
                
                a = random.randint(0, len(attributes)-1)
                
                mini = math.inf
                maxi = (-math.inf)
                
                for ix in idsR: # Getting the max and min values of the points in ix
                    data = self.dataSet.getData(ix)
                    if data[a] > maxi: maxi = data[a] 
                    if data[a] < mini: mini = data[a]
                    
                if margins[a] is None:
                    marginWidth = (maxi-mini) * proxAreaRange
                    margins[a] = marginWidth
                else:
                    marginWidth = margins[a]
                    
                v = random.uniform(mini, maxi)
                
                proxLeftBorder = max(mini, v - marginWidth)
                proxRightBorder = min(maxi, v + marginWidth)

                idsLeft = []
                idsRight = []
            
                
               # Assigning the data points to the left and right parts of the separation
                for ix in idsR:
                    if self.dataSet.getData(ix)[a] <= v:
                        idsLeft.append(ix)

                        if self.dataSet.getData(ix)[a] >= proxLeftBorder:
                            nbPtsNearLeft = nbPtsNearLeft +1

                    else:
                        idsRight.append(ix)

                        if self.dataSet.getData(ix)[a] <= proxRightBorder:
                            nbPtsNearRight = nbPtsNearRight +1
                            
                
                # Computing the ratio of the density of data points in the margin (left + right) and the density of data points in the node
                
                localDensity = (nbPtsNearLeft + nbPtsNearRight)

                areaDensity = len(idsR) 
                #areaDensity = self.forest.dataSet.nbPoints


                # Testing if the separation needs to be kept
                # Density threshold
                thresh = (self.forest.ALPHA * areaDensity) * self.forest.ETA
                
                while(localDensity > int(thresh)):
  
                    if excluded is None:
                        excluded = [None for _ in range(len(attributes))]
                        if excluded[a] is None:
                            excluded[a] = []
                        excluded[a].append(proxLeftBorder)    
                    else:
                        if excluded[a] is None:
                            excluded[a] = []
                        excluded[a].append(proxLeftBorder)
                    
                    values = excluded[a]
                    choices = [mini] + sorted(values) + [maxi]
                        
                    i = 0
                    while((i < (len(choices) - 1)) and ((choices[i] + 2 * marginWidth) >= choices[i+1])):
                        i = i+1
                    if (i == len(choices) - 1 ):
                        tested.append(a)
                        untestedAttributes = [j for j in range(len(attributes)) if j not in tested]
                        
                        if (len(tested) == len(attributes)):
                            
                            self.endNodes.append(idsR)
                            return Node(idsR, currDepth, None, None, None, None)
                        else:
                            
                            a = random.choice(untestedAttributes)
                            
                            mini = math.inf
                            maxi = (-math.inf)
                            
                            for ix in idsR: # Getting the max and min values of the points in ix
                                data = self.dataSet.getData(ix)
                                if data[a] > maxi: maxi = data[a] 
                                if data[a] < mini: mini = data[a]
                                
                            if margins[a] is None:
                                marginWidth = (maxi-mini) * proxAreaRange
                                margins[a] = marginWidth
                            else:
                                marginWidth = margins[a]
                                
                            v = random.uniform(mini, maxi)
                    else:
                        v = random.uniform(choices[i] + 2 * marginWidth, choices[i+1])
                
                    proxLeftBorder = max(mini, v - marginWidth)
                    proxRightBorder = min(maxi, v + marginWidth)
        
                    idsLeft = []
                    idsRight = []
                    nbPtsNearLeft=0 # Number of points in the left margin
                    nbPtsNearRight=0 # Number of points in the right margin
                    
                    # Assigning the data points to the left and right parts of the separation
                    for ix in idsR:
                        if self.dataSet.getData(ix)[a] <= v:
                            idsLeft.append(ix)
        
                            if self.dataSet.getData(ix)[a] >= proxLeftBorder:
                                nbPtsNearLeft = nbPtsNearLeft +1
        
                        else:
                            idsRight.append(ix)
        
                            if self.dataSet.getData(ix)[a] <= proxRightBorder:
                                nbPtsNearRight = nbPtsNearRight +1
                                
                    localDensity = (nbPtsNearLeft + nbPtsNearRight)
                        
                if (localDensity <= int(thresh)):
                    return Node(idsR, currDepth, a, v, self.build(np.array(idsLeft), currDepth + 1, margins=margins), self.build(np.array(idsRight), currDepth + 1, margins=margins))
        
        
        # Build a classic isolation tree
        else:
            attributes = self.dataSet.getAttributes()
            a = random.randint(0, len(attributes)-1)
            
            if len(idsR) <= 1 or currDepth >= self.forest.MAXHEIGHT:
                self.endNodes.append(idsR)
                return Node(idsR, currDepth, None, None, None,None)
            else:
                
                mini = math.inf
                maxi = (-math.inf)
                
                for ix in idsR: # Getting the max and min values of the points in ix
                    data = self.dataSet.getData(ix)
                    if data[a] > maxi: maxi = data[a] 
                    if data[a] < mini: mini = data[a]
                    
                v = random.uniform(mini, maxi)
                idsLeft = []
                idsRight = []         
    
                # Assigning the data points to the left and right parts of the separation
                for ix in idsR:
                    if self.dataSet.getData(ix)[a] <= v:
                        idsLeft.append(ix)
    
                    else:
                        idsRight.append(ix)
    
    
                return Node(idsR, currDepth, a, v, self.build(np.array(idsLeft), currDepth + 1), self.build(np.array(idsRight), currDepth + 1))           
  
    
    def pathLength(self, point, node, e):
        """
        Computes the path's length of a data point in the tree from Node node.
        To compute the path's length in the entire tree, set node to the root of the tree.
        Parameters
        ----------
        point : 1-D array of floats
            coordinates of the data point.
        node : Node
            the Node from which the search starts.
        e : integer
            current path length.
        Returns
        -------
        float
            length of the path of the data point.
        """
        if((node.leftNode is not None) or (node.rightNode is not None)):
            # (If it is an internal node)
            if point[node.sepAtt] < node.sepVal:
                return self.pathLength(point, node.leftNode, e+1)
            else:
                return self.pathLength(point, node.rightNode, e+1)
        else:
            # (If it is an external node)
            return e + c(len(node.ids))
        

        
    def getNode(self, point, node):
    
        
        while((node.leftNode is not None) and (node.rightNode is not None)):
            if point[node.sepAtt] < node.sepVal:
                return self.getNode(point, node.leftNode)
            else:
                return self.getNode(point, node.rightNode)
        return node
    
    
    def getNodeDescription(self, point, node, desc):
        
      #  mini = math.inf
      #  maxi = (-math.inf)
                    
        while((node.leftNode is not None) and (node.rightNode is not None)):
            attri = node.sepAtt
            value = node.sepVal
            
                    
            #if attri not in desc:
                
            #    for ix in node.ids: # Getting the max and min values of the points in ix
            #        data = self.dataSet.getData(ix)
            #        if data[attri] > maxi: maxi = data[attri] 
            #        if data[attri] < mini: mini = data[attri]
                #desc[attri] = [None, None]
            #    desc[attri] = [mini, maxi]
                
            if point[attri] < value:
                desc[attri][1] = value
                return self.getNodeDescription(point, node.leftNode, desc)
            else:
                desc[attri][0] = value
                return self.getNodeDescription(point, node.rightNode, desc)
        return desc
    
        
    def computeScoreModified(self, point):
        
        node = self.getNode(point, self.root)
        
        return 1- ((len(node.ids) - 1)/len(self.root.ids))
        
        # neighboursNumber = len(node.ids)
        
        # score = (1 - (neighboursNumber * (2 ** (node.depth - 1)))/len(self.root.ids))
        # if score < 0:
        #     return 0
        # else:
        #     return score
        
    def augmentedTree(self, points):
        
        treeEndNodes = self.endNodes
        endNodes = treeEndNodes.copy()
        k=0
        
        for i in range(len(points)):
            point = points[i]
            
            if i in self.ids:
                pass
            else:
                node = self.getNode(point, self.root)
                
                
                for key, n in enumerate(treeEndNodes):
                    if ((np.sort(node.ids) in np.sort(n)) and (len(node.ids) == len(n))):
                        k = k+1
                        endNodes[key] = np.append(endNodes[key], i)
        return endNodes


def c(n):
    """
    Computes the adjustment c(Size) : c(i) = ln(i) + Euler's constant
    
    Parameters
    ----------
    n : integer
        size of the sample.

    Returns
    -------
    float
        adjustment value.

    """
    if n > 2:
        return 2.0 * (np.log(n-1) + np.euler_gamma) - 2.0 * (n-1.0) / n
    elif n == 2:
        return 1.0
    else:
        return 0.0
        