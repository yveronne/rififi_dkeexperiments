#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Contains functions to visualize datasets and forests
@author: SHAMAN
"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.patches as patches

from .dataset import *
    

def viewDataset(d:Dataset):
    """
    Used to plot 2D datasets. For higher dimensional datasets, plots the first two dimensions.
    Parameters:
    -----------
    d : Dataset
        an instance of the Dataset class
    """
    x=list(d.data[:,0])
    y=list(d.data[:,1])  
    plt.scatter(x,y)
  


def viewDatasetWithAnomalies(d:Dataset, scores:np.ndarray, ALPHA:float, pp = plt, filters : np.ndarray =None):
    """
    Plots the dataset. Anomalies are in red. Regular instances are in blue,
    with an opacity proportional to their anomaly score
    
    Parameters:
    -----------
    d : Dataset
        an instance of the Dataset class
    scores : np.ndarray
        an array containing the anomaly scores
    ALPHA : float
        the anomaly threshold
    filters : np.ndarray
        the ids of the points to display (None means all points are displayed)
    """

    for pi in range(len(d.data)):
        if filters is None or pi in filters:
            if ALPHA != 0 and scores[pi] >= ALPHA:
                pp.plot([d.data[pi][0]], [d.data[pi][1]], "ro", alpha=scores[pi])
            else:
                pp.plot([d.data[pi][0]], [d.data[pi][1]], "bo", alpha=scores[pi])
    

def viewScores(d:Dataset, scores:np.ndarray, pp = plt, filters : np.ndarray =None):
    """
    Plots the dataset. 
    Opacity proportional to the anomaly scores

    Parameters:
    -----------
    d : Dataset
        an instance of the Dataset class
    scores : np.ndarray
        an array containing the anomaly scores
    filters : np.ndarray
        the ids of the points to display (None means all points are displayed)
    """

    for pi in range(len(d.data)):
        if filters is None or pi in filters:
            pp.plot([d.data[pi][0]], [d.data[pi][1]], "bo", alpha=scores[pi])
    


def drawTreeRec(nd, d, pl = None, maxiHeight=0, color = None):
    """
    Draw a tree for a 2D dataset
    
    Parameters:
    -----------
    nd : Node
        The root node
    d : Dataset
        the dataset
    """
    pp = plt
    if color is None:
        color = "black"
    if pl is not None:
        pp= pl
    #  pp.add_patch(Rectangle((nd.sepVal, miniY), maxiY-miniY , maxiX-miniX))

    if nd is not None and nd.sepAtt is not None:
        miniX = math.inf
        maxiX = (-math.inf)
        miniY = math.inf
        maxiY = (-math.inf)
        for id in nd.ids:
            if d.data[id][1] > maxiY:
                maxiY = d.data[id][1]
            if d.data[id][1] < miniY:
                miniY = d.data[id][1]
            if d.data[id][0] > maxiX:
                maxiX = d.data[id][0]
            if d.data[id][0] < miniX:
                miniX = d.data[id][0]

        if nd.sepAtt == 0:
            #pp.plot([nd.sepVal, nd.sepVal], [miniY, maxiY],color=color,alpha=(1 - nd.depth/maxiHeight))
            pp.plot([nd.sepVal, nd.sepVal], [miniY, maxiY], color=color, linewidth = maxiHeight/(nd.depth+1))
        else:
            #pp.plot([miniX,maxiX], [nd.sepVal,nd.sepVal],color=color,alpha=(1 - nd.depth/maxiHeight))
            pp.plot([miniX,maxiX], [nd.sepVal,nd.sepVal], color = color, linewidth = maxiHeight/(nd.depth+1))
        drawTreeRec(nd.leftNode,d,pp, maxiHeight, color)
        drawTreeRec(nd.rightNode,d,pp, maxiHeight, color)
        