# RIFIFI (+ DKE experiments)


## Project status

Ongoing...

## Description

This repository contains the sources of the RIFIFI algorithm, as described in the paper first submitted to EGC (Extraction et Gestion de Connaissances) then to DKE. The algorithm was implemented in Python.

## Content

The repository contains two folders:
- Data:
- Src: contains the sources of the project, spread into the following files...

## Requirements

The following packages are necessary:
- numpy
- matplotlib (for visualisations)


## Support

If you encounter any issue with code, please feel free to contact me at veronneyepmo@gmail.com.

## Roadmap

Next steps: clustering

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment

This research is part of the SEA DEFENDER project funded by the French DGA (Directorate General of Armaments).

## License
For open source projects, say how it is licensed.
